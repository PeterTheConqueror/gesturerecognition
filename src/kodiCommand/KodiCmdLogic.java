/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kodiCommand;

import java.util.function.Consumer;

/**
 *
 * @author Franz Berger <znarf.berger@gmail.com>
 */
public class KodiCmdLogic implements Consumer<Gesture> {

    private Consumer<KodiCmd> listener;
    private KodiCommands cmds;

    public KodiCmdLogic(Consumer<KodiCmd> listener) {
        this.listener = listener;
        cmds = KodiCommands.getKodiCommands();
    }

    @Override
    public void accept(Gesture g) {
        System.out.println("accepted gesture " + g.gesture.name());
        KodiCmd cmd = null;
        if (g.gesture.equals(Geste.CHANGE_ROOM)) {
            // send message in which room you currently are
            cmd=KodiCommands.getMsgCmd("You are in room", g.room.toString());
            listener.accept(cmd);
            cmd=KodiCommands.getMsgCmdTablet("You are in room", g.room.toString());

        } else {
            if (isGestureAvailable(g)) {
                cmd = cmds.getCmdWithRoom(g);
            } else {
                System.err.println(g.gesture + " not available in " + g.room);
            }
        }
        if (cmd != null) {
            listener.accept(cmd);
        }
    }

    private boolean isGestureAvailable(Gesture g) {
        return Geste.getGesteAvailable(g);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kodiCommand;

/**
 *
 * @author Franz Berger <znarf.berger@gmail.com>
 */
public enum Geste {

    UP("up"), DOWN("down"), SELECT("select"), LEFT("left"), RIGHT("right"), BACK("back"), HOME("home"), 
    PLAY("play"), TOGGLE("toggle"), STOP("stop"), NEXT("next"), PREVIOUS("previous"), STARTMSG("msgStart"),
    CHANGE_ROOM("change_room"), PAUSE("pause");

    private final String geste;

    private Geste(String geste) {
        this.geste = geste;
    }

    @Override
    public String toString() {
        return geste;
    }

    public static boolean getGesteAvailable(Gesture g) {
        boolean available = false;        

        switch (g.gesture) {
            case PLAY:
            case NEXT:
            case PREVIOUS:
            case STOP:
            case TOGGLE:
            case STARTMSG:
            case PAUSE:
                available = true;
                break;
        }

        switch (g.room) {
            case BAD:
            case WOHNZIMMER:
                switch (g.gesture) {
                    case UP:
                    case DOWN:
                    case LEFT:
                    case RIGHT:
                    case SELECT:
                    case HOME:
                    case BACK:
                        available = true;
                }
                break;
            case GANG:
                break;
            case SCHLAFZIMMER:
                break;
            case TRIGGER:
                break;
            case ABSTELLRAUM:
                break;
            case KUECHE:
                break;
            default:
                return false;

        }        
        return available;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kodiCommand;

/**
 *
 * @author Franz Berger <znarf.berger@gmail.com>
 */
public class KodiCmd {
    String topic;
    String cmd;

    public KodiCmd(String topic, String cmd) {
        this.topic = topic;
        this.cmd = cmd;
    }

    public String getTopic() {
        return topic;
    }

    public String getCmd() {
        return cmd;
    }
    
}

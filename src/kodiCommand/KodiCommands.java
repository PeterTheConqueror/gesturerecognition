/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kodiCommand;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Franz Berger <znarf.berger@gmail.com>
 */
public class KodiCommands {

    private static final String SEND_TOPIC = "command/api";
    private static final String SEND_TOPIC2 = "command/playbackstate";
    private static final String PREFIX = "kodi/";
    private static final String TABLET = "kodi/tablet/";
    private static Map<String, KodiCmd> cmds;
    private static KodiCommands cmd;

    public static KodiCommands getKodiCommands() {
        if (cmd == null) {
            cmd = new KodiCommands();
        }
        return cmd;
    }

    public static KodiCmd getVolCmd(int i) {
        return new KodiCmd(PREFIX+SEND_TOPIC, "{\"jsonrpc\":\"2.0\",\"method\":\"Application.SetVolume\",\"params\":{\"volume\":" + i + "},\"id\": 1}");
    }

    public static KodiCmd getMsgCmd(String title, String msg) {
        return new KodiCmd(PREFIX+SEND_TOPIC, "{\"jsonrpc\":\"2.0\",\"method\":\"GUI.ShowNotification\",\"params\":{\"title\":\"" + title + "\",\"message\":\"" + msg + "\"},\"id\":1}");
    }
    public static KodiCmd getVolCmdTablet(int i) {
        return new KodiCmd(TABLET+SEND_TOPIC, "{\"jsonrpc\":\"2.0\",\"method\":\"Application.SetVolume\",\"params\":{\"volume\":" + i + "},\"id\": 1}");
    }

    public static KodiCmd getMsgCmdTablet(String title, String msg) {
        return new KodiCmd(TABLET+SEND_TOPIC, "{\"jsonrpc\":\"2.0\",\"method\":\"GUI.ShowNotification\",\"params\":{\"title\":\"" + title + "\",\"message\":\"" + msg + "\"},\"id\":1}");
    }

    private KodiCommands() {
        cmds = new HashMap<>();
        cmds.put("right", new KodiCmd(SEND_TOPIC, "{\"jsonrpc\":\"2.0\",\"method\":\"Input.Right\",\"id\":1}"));
        cmds.put("left", new KodiCmd(SEND_TOPIC, "{\"jsonrpc\":\"2.0\",\"method\":\"Input.Left\",\"id\":1}"));
        cmds.put("up", new KodiCmd(SEND_TOPIC, "{\"jsonrpc\":\"2.0\",\"method\":\"Input.Up\",\"id\":1}"));
        cmds.put("down", new KodiCmd(SEND_TOPIC, "{\"jsonrpc\":\"2.0\",\"method\":\"Input.Down\",\"id\":1}"));
        cmds.put("back", new KodiCmd(SEND_TOPIC, "{\"jsonrpc\":\"2.0\",\"method\":\"Input.Back\",\"id\":1}"));
        cmds.put("select", new KodiCmd(SEND_TOPIC, "{\"jsonrpc\":\"2.0\",\"method\":\"Input.Select\",\"id\":1}"));
        cmds.put("home", new KodiCmd(SEND_TOPIC, "{\"jsonrpc\":\"2.0\",\"method\":\"Input.Home\",\"id\":1}"));
        cmds.put("msgStart", new KodiCmd(SEND_TOPIC, "{\"jsonrpc\":\"2.0\",\"method\":\"GUI.ShowNotification\",\"params\":{\"title\":\"Gesture Recognition\",\"message\":\"program started\"},\"id\":1}"));
        cmds.put("mute", new KodiCmd(SEND_TOPIC, "{\"jsonrpc\":\"2.0\",\"method\":\"Application.SetMute\",\"params\":{\"mute\":\"toggle\"}}"));
        cmds.put("toggle", new KodiCmd(SEND_TOPIC2, "toggle"));
        cmds.put("stop", new KodiCmd(SEND_TOPIC2, "0"));
        cmds.put("resume", new KodiCmd(SEND_TOPIC2, "1"));
        cmds.put("play", new KodiCmd(SEND_TOPIC2, "1"));
        cmds.put("pause", new KodiCmd(SEND_TOPIC2, "2"));
        cmds.put("next", new KodiCmd(SEND_TOPIC2, "next"));
        cmds.put("previous", new KodiCmd(SEND_TOPIC2, "previous"));
    }

    private KodiCmd getCmd(String cmd) {
//        KodiCmd intern = cmds.get(cmd);
//        System.out.println("get "+intern.topic);
//        return getCmd(intern, PREFIX);
        return cmds.get(cmd);
    }

    private KodiCmd getCmd(KodiCmd cmd, String prefix) {         
        return new KodiCmd(prefix+cmd.topic, cmd.cmd);
    }

    public KodiCmd getCmdWithRoom(Gesture g) {        
        KodiCmd cmd = getCmd(g.gesture.toString());
        if (g.room.equals(Room.WOHNZIMMER)) {
            cmd = getCmd(cmd, TABLET);
        } else {
            cmd =  getCmd(cmd, PREFIX);
        }
        return cmd;
    }

}

package kodiCommand;

import gesture.communication.MQTTGestureToKodiCmd;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static final String BROKER = "tcp://iot.soft.uni-linz.ac.at:1883";
    private static final String BROKER_HOME = "tcp://192.168.1.200:1883";

    private static final String RECEIVE_TOPIC = "gesture";
    private static final String RECEIVE_ROOM = "room";

    public static void main(String[] args) throws IOException {
//        MQTTGestureToKodiCmd mqtt = new MQTTGestureToKodiCmd(BROKER, RECEIVE_TOPIC, RECEIVE_ROOM);
        MQTTGestureToKodiCmd mqtt = new MQTTGestureToKodiCmd(BROKER_HOME, RECEIVE_TOPIC,RECEIVE_ROOM);
        KodiCmdLogic gest = new KodiCmdLogic(mqtt);
        mqtt.setListener(gest);
        System.out.println("Programm started: listening");

        try {
            Thread.sleep(500);
            gest.accept(new Gesture(Geste.STARTMSG,Room.BAD));            
            gest.accept(new Gesture(Geste.STARTMSG,Room.WOHNZIMMER));            
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}

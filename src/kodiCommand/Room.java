/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kodiCommand;

/**
 *
 * @author Franz Berger <znarf.berger@gmail.com>
 */
public enum Room {

    WOHNZIMMER("Wohnzimmer"),SCHLAFZIMMER("Schlafzimmer"),BAD("Bad"),GANG("Gang"),TRIGGER("Trigger"),KUECHE("Kueche"),VORZIMMER("Vorzimmer"),ABSTELLRAUM("Abstellraum");

    private final String room;
    

    private Room(String room) {
        this.room=room;
    }



}

package gesture.communication;

import java.util.function.Consumer;
import kodiCommand.Geste;
import kodiCommand.Gesture;
import kodiCommand.KodiCmd;
import kodiCommand.Room;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MQTTGestureToKodiCmd implements Consumer<KodiCmd> {

    private final int qos = 0;

    private boolean connected = false;

    private MqttAsyncClient sampleClient;
    private String broker;
    private String clientId;
    private Room room;
    private Room slidingWindow[] = new Room[3];
    public static String rescanTopic = "rescan";

    private final Room defRoom = Room.BAD;

    private Consumer<Gesture> listener;

    public MQTTGestureToKodiCmd(String broker, String receiveTopicGesture, String receiveTopicRoom) {

        this.broker = broker;

        this.clientId = MqttAsyncClient.generateClientId();
        this.listener = null;
        room = defRoom; // default - all gestures available
        for (Room r : slidingWindow) {
            r = defRoom;
        }
        MemoryPersistence persistence = new MemoryPersistence();
        try {
            sampleClient = new MqttAsyncClient(this.broker, this.clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);

            sampleClient.setCallback(new MqttCallback() {

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    if (receiveTopicGesture.equals(topic)) {
                        if (listener != null) {
                            System.out.println("msg arrived:\"" + message.toString() + "\"");

                            try {
                                Geste g = Geste.valueOf(message.toString().toUpperCase().replaceAll("[^A-Z]", ""));
                                listener.accept(new Gesture(g, room));
                            } catch (Exception ex) {
                                System.err.println("unknown gesture received: " + message.toString());
//                                ex.printStackTrace();
                            }

                        }
                    } else if (receiveTopicRoom.equals(topic)) {
                        try {
                            Room prev = room;
                            Room newRoom = Room.valueOf(message.toString().toUpperCase());
                            System.out.println("received room: "+newRoom);
                            slidingWindow[0] = slidingWindow[1];
                            slidingWindow[1] = slidingWindow[2];
                            slidingWindow[2] = newRoom;
                            if (newRoom.equals(slidingWindow[0]) && newRoom.equals(slidingWindow[1])) {
                                // room changed
                                if (!prev.equals(newRoom)) {
//                                    if(prev.equals(Room.WOHNZIMMER)) {
                                        listener.accept(new Gesture(Geste.PAUSE, prev));
//                                    }
                                    
                                    room = newRoom;
                                    if(room.equals(Room.BAD)||room.equals(Room.WOHNZIMMER)) {
                                        listener.accept(new Gesture(Geste.PLAY, room));
                                    }
                                    listener.accept(new Gesture(Geste.CHANGE_ROOM, room));
                                    
                                }
                            } else {
                                // scan new to ensure that room is correct
                                // send command to µc
                                sampleClient.publish(rescanTopic, new MqttMessage("r".getBytes()));
                            }
                            System.out.println("desided on room:"+room);
                        } catch (IllegalArgumentException | NullPointerException e) {
                            // Room not found-> switch to default room
                            room = defRoom;
                            e.printStackTrace();
                        }
                    }

                }

                @Override

    public void deliveryComplete(IMqttDeliveryToken token) {
        System.out.println("deliveryComplete");
    }

    @Override
    public void connectionLost(Throwable cause) {
        System.err.println("connectionLost");
    }
}
);

            sampleClient.connect(connOpts, null, new IMqttActionListener() {

                @Override
        public void onSuccess(IMqttToken asyncActionToken) {
                    try {
                        sampleClient.subscribe(receiveTopicGesture, qos);
                        sampleClient.subscribe(receiveTopicRoom, qos);
                    } catch (MqttException e) {
                        System.err.println("error subscribing to topic, " + e.getMessage());
                    }
                    connected = true;
                }

                @Override
        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    System.err.println("failed to connect, " + exception.getMessage());
                    connected = false;
                }
            });
        } catch (MqttException me) {
            System.err.println(me.getMessage());
        }

    }

    @Override
        public void accept(KodiCmd cmd) {
        try {
            sendCmd(cmd);
        } catch (MqttException ex) {
            System.err.println("MqttException: " + ex.getMessage());
        }
    }

    private void sendCmd(KodiCmd cmd) throws MqttException {
        if (connected) {
            System.out.println("sending " + cmd.getCmd() + " to " + cmd.getTopic());
            sampleClient.publish(cmd.getTopic(), new MqttMessage(cmd.getCmd().getBytes()));
        } else {
            System.err.println("connect first before sending");
        }
    }

    public void setListener(Consumer<Gesture> listener) {
        if (this.listener == null) {
            this.listener = listener;
        } else {
            System.err.println("Error: Only single listener possible");
        }
    }
}

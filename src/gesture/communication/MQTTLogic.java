package gesture.communication;

import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MQTTLogic implements Consumer<String> {

    private final static String PATTERN = "\\[(-?\\d{1,2}\\.\\d{2,8},){3}\\]";

    private String stringBuffer;

    private final int qos = 0;

    private boolean connected = false;
    private final String sendTopic;
    private MqttAsyncClient sampleClient;
    private String broker;
    private String clientId;

    private Consumer<String> listener;

    public MQTTLogic(String broker, String receiveTopic, String sendTopic) {
        this.stringBuffer = "";
        this.broker = broker;
        this.sendTopic = sendTopic;
        this.clientId = MqttAsyncClient.generateClientId();
        this.listener = null;
        MemoryPersistence persistence = new MemoryPersistence();
        try {
            sampleClient = new MqttAsyncClient(this.broker, this.clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            //connOpts.setConnectionTimeout(10000);
//            connOpts.setKeepAliveInterval(3600);
            connOpts.setCleanSession(true);

            sampleClient.setCallback(new MqttCallback() {

                @Override
                public void messageArrived(String topic, MqttMessage message) {
                    bufferAndSendMessage(message.toString());
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    System.out.println("deliveryComplete");
                }

                @Override
                public void connectionLost(Throwable cause) {
                    System.err.println("connectionLost");
                    cause.printStackTrace();
                    System.err.println(cause.toString());
                }
            });

            sampleClient.connect(connOpts, null, new IMqttActionListener() {

                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    try {
                        sampleClient.subscribe(receiveTopic, qos);
                    } catch (MqttException e) {
                        System.err.println("error subscribing to topic, " + e.getMessage());
                    }
                    connected = true;
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    System.err.println("failed to connect, " + exception.getMessage());
                    connected = false;
                    exception.printStackTrace();
                }
            });
        } catch (MqttException me) {
            System.err.println(me.getMessage());
            me.printStackTrace();
        }

    }

    public void bufferAndSendMessage(String s) {
        stringBuffer += s;
        if (listener != null) {
            if (stringBuffer.contains("s")) {
                stringBuffer = stringBuffer.replace("s", "");
                try {
                    sampleClient.publish(sendTopic, new MqttMessage("select".getBytes()));
                } catch (MqttException ex) {
                    Logger.getLogger(MQTTLogic.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (stringBuffer.contains("b")) {
                stringBuffer = stringBuffer.replace("b", "");
                try {
                    sampleClient.publish(sendTopic, new MqttMessage("back".getBytes()));
                } catch (MqttException ex) {
                    Logger.getLogger(MQTTLogic.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            while (stringBuffer.contains("]")) {
                if (stringBuffer.contains("[")) {
                    int start = stringBuffer.indexOf("[");
                    int end = stringBuffer.indexOf("]", start + 1);
                    if (start < stringBuffer.length() && end < stringBuffer.length() && start >= 0 && end >= 0) {
                        try {
                            String str = stringBuffer.substring(start, end + 1);
                            if (str.matches(PATTERN)) {
                                listener.accept(stringBuffer.substring(start + 1, end - 1)); // old version
                            } else {
                                System.err.println("error: " + str);
                            }

                            stringBuffer = stringBuffer.substring(end + 1);
                        } catch (StringIndexOutOfBoundsException ex) {
                            System.err.println("error occured start:" + start + " end" + end);
                            ex.printStackTrace();
                        }
                    }
                } else {
                    break;
                }
            }
        }

    }

    @Override
    public void accept(String s) {
//        System.out.println(s + " recognized!!!");
        try {
            sendString(s);
        } catch (MqttException ex) {
            System.err.println("MqttException: " + ex.getMessage());
        }
    }

    private void sendString(String s) throws MqttException {
        s += "\n";
        if (connected) {
            sampleClient.publish(sendTopic, new MqttMessage(s.getBytes()));
        } else {
            System.err.println("connect first before sending");
        }
    }

    public void setListener(Consumer<String> listener) {
        if (this.listener == null) {
            this.listener = listener;
        } else {
            System.err.println("Error: Only single listener possible");
        }
    }
}

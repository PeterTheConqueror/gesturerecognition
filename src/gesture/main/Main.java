package gesture.main;

import gesture.recognition.GestureLogic;
import gesture.communication.MQTTLogic;
import java.io.IOException;

public class Main {
    
    private static final String BROKER = "tcp://192.168.1.200:1883";

    private static final String RECEIVE_TOPIC = "acc";
    private static final String SEND_TOPIC = "gesture";

//    private static final int WINDOW_SIZE = 20;

//    private static final double SIMILARITY_THRESHOLD = 60;

    /**
     * How many accelerometer values should be skipped after recognizing a
     * gesture
     *
     * Since we send one set of values every 50 ms, a skip value of 10
     * corresponds to a 500 ms pause between gestures
     */
    private static final int SKIP = 10;

    public static void main(String[] args) throws IOException {
        MQTTLogic mqtt = new MQTTLogic(BROKER, RECEIVE_TOPIC, SEND_TOPIC);
        GestureLogic gest = new GestureLogic(mqtt
//                , SIMILARITY_THRESHOLD
                , SKIP
//                , WINDOW_SIZE
        );
        mqtt.setListener(gest);
        System.out.println("gesture recognition started");
    }

}

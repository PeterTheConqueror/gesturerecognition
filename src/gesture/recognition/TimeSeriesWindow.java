package gesture.recognition;

import net.sf.javaml.distance.fastdtw.timeseries.TimeSeries;
import net.sf.javaml.distance.fastdtw.timeseries.TimeSeriesPoint;

// Simple linked list with maximum size and method to create a TimeSeries
public class TimeSeriesWindow {

    private final int windowSize;
    private int size = 0;

    private final Node head = new Node(null);
    private Node tail = head;

    public TimeSeriesWindow(int windowSize) {
        this.windowSize = windowSize;
    }

    public void removeFirst() {
        if (head.next != null) {
            head.next = head.next.next;
            size--;
        }
    }

    public void add(double[] v) {
        tail = (tail.next = new Node(v));
        if (size == windowSize) {
            head.next = head.next.next;
        } else {
            size++;
        }
    }

    boolean isFull() {
        return size == windowSize;
    }

    TimeSeries getTimeSeries() {
        TimeSeries ts = new TimeSeries(3);
        int idx = 1;
        for (Node current = head.next; current != null; current = current.next) {
            ts.addLast(idx, new TimeSeriesPoint(current.v));
            idx++;
        }
        ts.setLabels(new String[]{"Time"});
        return ts;
    }

    private static class Node {

        private final double[] v;
        Node next = null;

        public Node(double[] v) {
            this.v = v;
        }
    }

}

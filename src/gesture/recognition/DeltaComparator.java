/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gesture.recognition;

/**
 *
 * @author william
 */
public class DeltaComparator {

    private final int skip;

    public DeltaComparator(int skip) {
        this.skip = skip;
    }

    private int skipCount = 0;
    private final double[] VALUES = new double[]{0, 0, 10};

    public int compare(double[] newValues) {
        int res = 0;
        if (skipCount <= 0) {
            double threshold = 6;
            for (int i = 0; i < newValues.length; i++) {
                double diff = (newValues[i] - VALUES[i]);
                double absDiff = Math.abs(diff);
                if (absDiff > threshold) {
                    int sign = (int) Math.signum(diff);
                    threshold = absDiff;
                    res = sign * (i + 1);
                }
            }
            if (res != 0) {
                skipCount = skip;
            }
        }
        skipCount--;
        for (int i = 0; i < VALUES.length; i++) {
            VALUES[i] = newValues[i];
        }
        return res;
    }

}

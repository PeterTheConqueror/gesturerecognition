package gesture.recognition;

import java.util.function.Consumer;

public class GestureLogic implements Consumer<String> {

    private final Consumer<String> listener;

    private final DeltaComparator DELTA_COMP;

//    private final TimeSeries[][] gestures;
//    private final String[] gestureNames;
//    private final TimeSeriesWindow WINDOW;
//    private final double similarityThreshold;
    /**
     * How many accelerometer values should be skipped after recognizing a
     * gesture
     *
     * Since we send one set of values every 50 ms, a skip value of 10
     * corresponds to a 500 ms pause between gestures
     */
//    private final int skip;
    public GestureLogic(Consumer<String> listener
//            , double similarityThreshold
            , int skip
//           , int windowSize
    ) {
        this.listener = listener;
        this.DELTA_COMP = new DeltaComparator(skip);
//        this.similarityThreshold = similarityThreshold;
//        this.skip = skip;
//        // init window
//        this.WINDOW = new TimeSeriesWindow(windowSize);
        // init gestures
//        File[] gestDirs = new File(".")
//                .listFiles(x -> x.getName()
//                        .startsWith("gest"));
//        Arrays.sort(gestDirs);
//        gestureNames = new String[gestDirs.length];
//        for (int i = 0; i < gestDirs.length; i++) {
//            File gestDir = gestDirs[i];
//            gestureNames[i] = gestDir.getName().substring(4).toLowerCase();
//        }
//        List<Integer> sizes = new LinkedList<>();
//        for (File gestDir : gestDirs) {
//            String[] gf = gestDir.list((f, d) -> !f.getName().endsWith(".csv"));
//            sizes.add(gf.length);
//        }
//        gestures = new TimeSeries[sizes.size()][];
//        for (int i = 0; i < sizes.size(); i++) {
//            int size = sizes.get(i);
//            gestures[i] = new TimeSeries[size];
//        }
//        int gestId = 0;
//        for (File gestDir : gestDirs) {
//            String dirStr = gestDir.toString();
//            String[] gestFiles = gestDir.list((f, d) -> !f.getName().endsWith(".csv"));
//            int idx = 0;
//            Arrays.sort(gestFiles);
//            for (String gestFile : gestFiles) {
//                TimeSeries t = new TimeSeries(dirStr + '/' + gestFile, ',');
//                gestures[gestId][idx] = t;
//                System.out.println(dirStr + '/' + gestFile + " " + gestId + " / " + idx);
//                idx++;
//            }
//            gestId++;
//        }
    }

//    private void addToWindow(String line) {
//        // Add new element
//        WINDOW.add(convertToArray(line));
//        // If window is full, check for similarity to gestures
//        if (WINDOW.isFull()) {
//            // gest is the idx of an identified gesture or -1 if no gesture could be identified
//            int gest = checkSimilarities();
//            // if a gesture was recognized
//            if (gest != -1) {
//                // send it
//                sendGesture(gest);
//                // And remove SKIP entries from the window to prevent double-triggers
//                for (int i = 0; i < skip; i++) {
//                    WINDOW.removeFirst();
//                }
//            }
//        }
//    }
    private double[] convertToArray(String line) {
        String[] split = line.split(",");
        double[] value = new double[split.length];
        for (int i = 0; i < split.length; i++) {
            value[i] = Double.parseDouble(split[i]);
        }
        return value;
    }

//    private void sendGesture(int gest) {
//        listener.accept(gestureNames[gest]);
//    }
//    private int checkSimilarities() {
//        TimeSeries ts = WINDOW.getTimeSeries();
//        // Maximal similarity is when the warp distance is lowest
//        double maxSim = similarityThreshold;
//        int maxInd = -1;
//        int subIdx = 0;
//        for (int i = 0; i < gestures.length; i++) {
//            TimeSeries[] gesture = gestures[i];
//            int fIdx = 0;
//            for (TimeSeries compTs : gesture) {
//                double sim = FastDTW.getWarpDistBetween(ts, compTs);
//                if (sim < maxSim) {
//                    maxSim = sim;
//                    maxInd = i;
//                    subIdx = fIdx;
//                }
//                fIdx++;
//            }
//        }
//        if (maxInd != -1) {
//            System.out.println();
//            System.out.println(maxSim + " / " + maxInd + " / " + subIdx);
//            System.out.println();
//        }
//        return maxInd;
//    }
    @Override
    public void accept(String t) {
//        addToWindow(t);
        int res = DELTA_COMP.compare(convertToArray(t));
        switch (res) {
            case -1:
                listener.accept("next");
                break;
            case -2:
                listener.accept("right");
                break;
            case -3:
                listener.accept("down");
                break;
            case 1:
                listener.accept("toggle");
                break;
            case 2:
                listener.accept("left");
                break;
            case 3:
                listener.accept("up");
                break;
        }
    }

}

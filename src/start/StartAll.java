/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package start;


/**
 *
 * @author Franz Berger <znarf.berger@gmail.com>
 */
public class StartAll {
    public static void main(String[] args) {
        try {
            gesture.main.Main.main(args);
            
            kodiCommand.Main.main(args);
            roomRecgonition.MqttClient.main(args);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
    }
    
}

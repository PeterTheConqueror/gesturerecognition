package roomRecgonition;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import weka.classifiers.Classifier;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.ConverterUtils.DataSource;

public class MqttClient {

    private final int qos = 0;

    private boolean connected;
    private String sendTopic;
    private MqttAsyncClient sampleClient;
    private String broker;
    private String clientId;
    private Collection<String> topics;
	// private File file;
    // private BufferedWriter writer;

    private Classifier classif;
    private Instances instances;

    private Map<String, Integer> tuppel = new HashMap<>();
    private final String[] AP_NAMES = {"3HuiGate_2.4GHz_5302", "3HuiTube_2.4Ghz_AA1E", "3HuiTube_2.4Ghz_CC58",
			"3WebCube0ECF", "3WebCube8800", "3WebCube9B34", "3WebCubeB7ED", "3WebCubeCD9A", "3WebCubeDB6B", "A1-04AC91",
			"A1-115551", "A1-14CBEF", "A1-52CBCA", "A1-6898AF", "A1-A55037", "Boobs", "CSC-Linz-Training-WiFi",
			"D-Link", "DIRECT-9B-HP-ENVY-5540-series", "DIRECT-fb-HP-M477-LaserJet", "DKP", "EDBLINZ", "EFLIHSDIA",
			"Funkueberwachung-5", "HP-Print-DB-Officejet-Pro-8620", "HP-Print-E8-Photosmart",
			"HP-Print-E8-Photosmart-5520", "JOERG2104", "KUni-Linz", "KUni-Linz-Institut-BiKu",
			"KUni-Linz-Institut-RuD", "LNKOOE", "LOVE4EVER", "Lehrer", "Liwest0222", "Liwest10C2", "Liwest1722",
			"Liwest1852", "Liwest2C9A", "Liwest5E52", "Liwest60F2", "Liwest6BEA", "Liwest74CA", "Liwest7EF2",
			"Liwest8C9A", "Liwest8EC2", "Liwest9F3A", "LiwestA5B2", "LiwestCF6A", "LiwestCF82", "LiwestD0BA",
			"LiwestD3E2", "Liwest_ad4ed8", "Martin_router_king", "Mobility_Team_CSC", "NETGEAR24", "NETGEAR38",
			"NETGEAR63", "NETGEAR77", "NG150_EXT", "Pauli", "RAUSCHERNET", "ReaktivZentrale", "RuufNetz_EXT", "SBL",
			"Schule", "SpeedTouchC20B4D", "SweetNet", "TMOBILE-25748", "TMOBILE-70020", "TP-LINK_2656CA",
			"Thomson2FE335", "WLAN.Tele2.net", "WLAN1-001083", "WLTWO", "beate", "dOqRfFzvvhgG", "luca" };

    public MqttClient() throws Exception {
        //this("tcp://10.0.0.254:1883", MqttAsyncClient.generateClientId(), Collections.singleton("rssi"), null);
        this("tcp://192.168.1.200:1883", MqttAsyncClient.generateClientId(), Collections.singleton("rssi"), null);
    }

    public MqttClient(String broker, String clientId, Collection<String> topics, String sendTopic) throws Exception {
        if (topics != null && topics.size() == 1 && sendTopic == null) {
            sendTopic = topics.iterator().next();
        }
        if (clientId == null || sendTopic == null || broker == null || topics == null || broker.isEmpty()
                || clientId.isEmpty() || sendTopic.isEmpty()) {
            throw new RuntimeException(String.format("invalid configuration broker: %s  clientId: %s  topics: %s ",
                    broker, clientId, topics));
        }
        this.sendTopic = sendTopic;
        this.broker = broker;
        this.clientId = clientId;
        this.topics = topics;

        classif = loadClassifier("classifier\\RF.model");
		//classif = loadClassifier("classifier\\SMO.model");
        //classif = loadClassifier("classifier\\CENTER_NORM_RF.model");
        // SMO_NOISE_CENTER_NORM
        DataSource source = new DataSource("data\\rooms.arff");

        instances = source.getDataSet();
        if (instances.classIndex() == -1) {
            instances.setClassIndex(instances.numAttributes() - 1);
        }

		// //classif.buildClassifier(instances);
		// file = new File("C:\\Users\\Paul\\Desktop\\Trigger_Einang.txt");
        // writer = new BufferedWriter(new OutputStreamWriter(new
        // FileOutputStream(file)));
        setup();
    }

    private void setup() {
        MemoryPersistence persistence = new MemoryPersistence();
        try {
            sampleClient = new MqttAsyncClient(this.broker, this.clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);

            sampleClient.setCallback(new MqttCallback() {

                public void messageArrived(String topic, MqttMessage message) throws Exception {

					// System.out.println(message.toString());
                    String raws[] = message.toString().split("\n");
                    for (String raw : raws) {
                        if (raw.equals("begin")) {
                            tuppel = new HashMap<>();
                            for (String s : AP_NAMES) {
                                tuppel.put(s, -100);
                            }
                        } else if (raw.equals("end")) {

                            Instance i = new DenseInstance(instances.numAttributes());
                            i.setDataset(instances);

                            for (int j = 0; j < AP_NAMES.length; j++) {
								// System.out.println(instances.attribute(j).name().equals(AP_NAMES[j])+"
                                // "+tuppel.get(AP_NAMES[j]));
                                i.setValue(j, tuppel.get(AP_NAMES[j]));
                            }

                            double[] lab = classif.distributionForInstance(i);
                            String room = instances.classAttribute().value((int) classif.classifyInstance(i));
                            System.out.println(room);
                            
                            sampleClient.publish("room", new MqttMessage(room.getBytes()));
                        } else {
                            String name = "";
                            int value = 0;

                            String[] parts = raw.split(" ");
                            if (parts.length > 2) {
                                for (int i = 0; i < parts.length - 2; i++) {
                                    name += parts[i] + "-";
                                }
                                name += parts[parts.length - 2];
                                value = Integer.parseInt(parts[parts.length - 1]);
                            } else if (parts.length == 2) {
                                name = parts[0];
                                value = Integer.parseInt(parts[1]);
                            } else {
                                System.err.println("unknown instance");
                            }

                            if (tuppel.containsKey(name)) {
                                tuppel.put(name, value);
                            }
                        }
                    }
					// writer.write(message.toString());
                    // writer.write("\n");

                }

                public void deliveryComplete(IMqttDeliveryToken token) {
                }

                public void connectionLost(Throwable cause) {
                }
            });

            sampleClient.connect(connOpts, null, new IMqttActionListener() {

                public void onSuccess(IMqttToken asyncActionToken) {
                    for (String topic : MqttClient.this.topics) {
                        try {
                            sampleClient.subscribe(topic, qos);
                        } catch (MqttException e) {
                        }
                    }
                    connected = true;
                }

                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    connected = false;
                }
            });
        } catch (MqttException me) {
            processEx(me);
        }
    }

    private static void processEx(MqttException me) {
    }

    public void close() throws IOException {
        if (connected) {
            try {
                sampleClient.disconnect();
                // writer.close();
            } catch (MqttException e) {
                processEx(e);
            }
        } else {
        }
    }

    public static void main(String[] args) throws Exception {
        MqttClient connector = new MqttClient();

        BufferedReader re = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("started room recog. end with e");
        while (!re.readLine().equals("e")) {

        }
        connector.close();
        re.close();
    }

    public static Classifier loadClassifier(String uri) throws FileNotFoundException, Exception {

        return (Classifier) SerializationHelper.read(new FileInputStream(uri));

    }

}
